package pl.cybul.blog

import com.blogspot.toomuchcoding.spock.subjcollabs.Collaborator
import com.blogspot.toomuchcoding.spock.subjcollabs.Subject
import spock.lang.Specification

import static pl.cybul.blog.DataHelper.createBlogEntryReview

class DeleteReviewCommandHandlerSpec extends Specification {

    @Subject
    DeleteReviewCommandHandler subject

    @Collaborator
    BlogRepository blogRepository = Mock()
    @Collaborator
    BlogEvents blogEvents = Mock()
    @Collaborator
    ObjectFactory objectFactory = new ObjectFactory()

    def "Should throw DomainError for non existing entry"() {
        given:
        def entryUuid = UUID.randomUUID()
        def reviewUuid = UUID.randomUUID()
        def command = new DeleteReview(entryUuid, reviewUuid)
        blogRepository.getById(entryUuid) >> BlogEntry.EMPTY

        when:
        subject.handle(command)

        then:
        thrown(DomainError)
    }

    def "Should throw DomainError for archived entry"() {
        given:
        def entryUuid = UUID.randomUUID()
        def reviewUuid = UUID.randomUUID()
        def command = new DeleteReview(entryUuid, reviewUuid)

        and:
        def entry = new BlogEntry("title", "content")
        entry.@entryId = entryUuid
        entry.@state = EntryState.ARCHIVED
        blogRepository.getById(entryUuid) >> entry

        when:
        subject.handle(command)

        then:
        thrown(DomainError)
    }

    def "Should throw DomainError for entry with no existing review"() {
        given:
        def entryUuid = UUID.randomUUID()
        def reviewUuid = UUID.randomUUID()
        def command = new DeleteReview(entryUuid, reviewUuid)

        and:
        def entry = new BlogEntry("title", "content")
        entry.@entryId = entryUuid
        entry.@state = EntryState.ACTIVE
        entry.@reviews = []
        blogRepository.getById(entryUuid) >> entry

        when:
        subject.handle(command)

        then:
        thrown(DomainError)
    }

    def "Should remove a review, recalculate rating, persist and publish an event"() {
        given:
        def entryUuid = UUID.randomUUID()
        def reviewUuid = UUID.randomUUID()
        def command = new DeleteReview(entryUuid, reviewUuid)

        and:
        def otherReviewUUid = UUID.randomUUID()

        def entry = new BlogEntry("title", "content")
        entry.@entryId = entryUuid
        entry.@state = EntryState.ACTIVE
        entry.@reviews = [createBlogEntryReview(reviewUuid, 2), createBlogEntryReview(otherReviewUUid, 4)]
        entry.@averageRating = 3d
        blogRepository.getById(entryUuid) >> entry

        when:
        subject.handle(command)

        then:
        entry.getReviews().size() == 1
        entry.getAverageRating() == 4
        1 * blogRepository.persist(entry)
        1 * blogEvents.publish(_ as BlogEntryReviewDeleted)
    }

}
