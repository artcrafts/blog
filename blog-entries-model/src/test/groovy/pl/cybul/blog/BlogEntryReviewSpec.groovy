package pl.cybul.blog

import spock.lang.Specification
import spock.lang.Unroll

class BlogEntryReviewSpec extends Specification {

    @Unroll
    def "Should throw the DomainError for #title_msg, #content_msg, #rate_msg "(){
        when:
        def review = new BlogEntryReview(title, content, rating)

        then:
        thrown(DomainError)

        where:
        title   | content | rating
        null    | "value" | 1
        "value" | null    | 1
        "value" | "value" | null
        "value" | "value" | 0
        "value" | "value" | 11
        "value" | "value" | 111

        rate_msg = (10 <= rating) && (rating > 0) ? 'rating in 1-10 range' : 'rating out of 1-10 range'
        content_msg = content == null ? 'nullable content' : 'not nullable content'
        title_msg = content == null ? 'nullable title' : 'not nullable title'
    }


    @Unroll
    def "Should create a valid BlogEntryReview object"() {
        given:
        def title = "title"
        def content = "content"

        when:
        def review = new BlogEntryReview(title, content, rating)

        then:
        review.content == content
        review.title == title
        review.rating == rating
        review.reviewId

        where:
        rating | _
        1      | _
        9      | _
        10     | _
    }
}
