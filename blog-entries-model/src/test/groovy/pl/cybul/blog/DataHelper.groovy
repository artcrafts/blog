package pl.cybul.blog


static BlogEntryReview createBlogEntryReview(uuid, rating) {
    def review = new BlogEntryReview("title", "content", rating)
    review.@reviewId = uuid
    return review
}
