package pl.cybul.blog

import com.blogspot.toomuchcoding.spock.subjcollabs.Collaborator
import com.blogspot.toomuchcoding.spock.subjcollabs.Subject
import spock.lang.Specification

class CreateEntryCommandHandlerSpec extends Specification {

    @Subject
    CreateEntryCommandHandler subject

    @Collaborator
    BlogRepository blogRepository = Mock()
    @Collaborator
    BlogEvents blogEvents = Mock()
    @Collaborator
    ObjectFactory objectFactory = new ObjectFactory()

    def "Should persist created entry and publish an event"() {
        given:
        def command = new CreateEntry("title", "content")

        when:
        subject.handle(command)

        then:
        1 * blogRepository.persist(_ as BlogEntry)
        1 * blogEvents.publish(_ as BlogEntryCreated)
    }
}
