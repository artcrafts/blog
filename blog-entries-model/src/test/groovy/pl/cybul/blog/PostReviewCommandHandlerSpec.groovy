package pl.cybul.blog

import com.blogspot.toomuchcoding.spock.subjcollabs.Collaborator
import com.blogspot.toomuchcoding.spock.subjcollabs.Subject
import spock.lang.Specification

import static pl.cybul.blog.DataHelper.createBlogEntryReview

class PostReviewCommandHandlerSpec extends Specification {

    @Subject
    PostReviewCommandHandler subject

    @Collaborator
    BlogRepository blogRepository = Mock()
    @Collaborator
    BlogEvents blogEvents = Mock()
    @Collaborator
    ObjectFactory objectFactory = new ObjectFactory()

    def "Should throw DomainError for non existing entry"() {
        given:
        def entryUuid = UUID.randomUUID()
        def command = new PostReview(entryUuid, "title", "content", 1)
        blogRepository.getById(entryUuid) >> BlogEntry.EMPTY

        when:
        subject.handle(command)

        then:
        thrown(DomainError)
    }

    def "Should throw DomainError for archived entry"() {
        given:
        def entryUuid = UUID.randomUUID()
        def command = new PostReview(entryUuid, "title", "content", 1)

        and:
        def entry = new BlogEntry("title", "content")
        entry.@entryId = entryUuid
        entry.@state = EntryState.ARCHIVED
        blogRepository.getById(entryUuid) >> entry

        when:
        subject.handle(command)

        then:
        thrown(DomainError)
    }

    def "Should add a review, recalculate rating, persist and publish an event"() {
        given:
        def entryUuid = UUID.randomUUID()
        def command = new PostReview(entryUuid, "title", "content", 4)

        and:
        def otherReviewUUid = UUID.randomUUID()

        def entry = new BlogEntry("title", "content")
        entry.@entryId = entryUuid
        entry.@state = EntryState.ACTIVE
        entry.@reviews = [createBlogEntryReview(otherReviewUUid, 8)]
        entry.@averageRating = 8d
        blogRepository.getById(entryUuid) >> entry

        when:
        subject.handle(command)

        then:
        entry.getReviews().size() == 2
        entry.getAverageRating() == 6
        1 * blogRepository.persist(entry)
        1 * blogEvents.publish(_ as BlogEntryReviewed)
    }
}
