package pl.cybul.blog

import com.blogspot.toomuchcoding.spock.subjcollabs.Collaborator
import com.blogspot.toomuchcoding.spock.subjcollabs.Subject
import spock.lang.Specification

class ArchiveEntryCommandHandlerSpec extends Specification {

    @Subject
    ArchiveEntryCommandHandler subject

    @Collaborator
    BlogRepository blogRepository = Mock()
    @Collaborator
    BlogEvents blogEvents = Mock()

    def "Should throw DomainError for non existing entry"() {
        given:
        def uuid = UUID.randomUUID()
        def command = new ArchiveEntry(uuid)
        blogRepository.getById(uuid) >> BlogEntry.EMPTY

        when:
        subject.handle(command)

        then:
        thrown(DomainError)
    }

    def "Should throw DomainError for already archived entry"() {
        given:
        def uuid = UUID.randomUUID()
        def command = new ArchiveEntry(uuid)

        and:
        def entry = new BlogEntry("title", "content")
        entry.@entryId = uuid
        entry.@state = EntryState.ARCHIVED
        blogRepository.getById(uuid) >> entry

        when:
        subject.handle(command)

        then:
        thrown(DomainError)
    }

    def "Should persist archived entry and publish an event"() {
        given:
        def uuid = UUID.randomUUID()
        def command = new ArchiveEntry(uuid)

        and:
        def entry = new BlogEntry("title", "content")
        entry.@entryId = uuid
        blogRepository.getById(uuid) >> entry

        when:
        subject.handle(command)

        then:
        entry.getState() == EntryState.ARCHIVED
        1 * blogRepository.persist(entry)
        1 * blogEvents.publish(_ as BlogEntryArchived)
    }
}
