package pl.cybul.blog;

class ArchivePolicy {

  static boolean archivePossible(BlogEntry entry) {
    return !entry.getState().isArchived();
  }
}
