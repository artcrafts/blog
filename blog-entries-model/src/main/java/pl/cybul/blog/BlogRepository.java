package pl.cybul.blog;

import java.util.List;
import java.util.UUID;

public interface BlogRepository {

  void persist(BlogEntry blogEntry);

  BlogEntry getById(UUID entryId);

  List<BlogEntry> getAll();
}
