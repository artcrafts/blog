package pl.cybul.blog;

class ReviewPolicy {

  static boolean reviewAddPossible(BlogEntry entry) {
    return !entry.isArchived();
  }

  static boolean reviewDeletionPossible(BlogEntry entry) {
    return !entry.isArchived();
  }
}
