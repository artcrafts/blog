package pl.cybul.blog;

import pl.cybul.blog.cqrs.Event;

public interface BlogEvents {

  void publish(Event event);
}
