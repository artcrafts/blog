package pl.cybul.blog;

import lombok.RequiredArgsConstructor;
import pl.cybul.blog.cqrs.CommandHandler;

@RequiredArgsConstructor
public class CreateEntryCommandHandler implements CommandHandler<CreateEntry> {

  private final BlogRepository blogRepository;
  private final BlogEvents blogEvents;
  private final ObjectFactory objectFactory;

  @Override
  public void handle(CreateEntry command) {
    BlogEntry entry = new BlogEntry(command.getTitle(), command.getContent());
    blogRepository.persist(entry);
    blogEvents.publish(objectFactory.create(entry));
  }
}
