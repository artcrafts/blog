package pl.cybul.blog;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;
import pl.cybul.blog.cqrs.Event;

import java.time.LocalDateTime;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@ToString
@Value
public class BlogEntryArchived extends Event {

  private final UUID entryId;
  private final LocalDateTime archiveDate;

  @Override
  public UUID getId() {
    return entryId;
  }
}
