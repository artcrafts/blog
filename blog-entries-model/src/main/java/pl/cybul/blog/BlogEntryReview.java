package pl.cybul.blog;

import lombok.Getter;

import java.util.UUID;

@Getter
public class BlogEntryReview {

  private UUID reviewId;
  private String title;
  private String content;
  private Integer rating;

  public BlogEntryReview(String title, String content, Integer rating) {
    this.reviewId = UUID.randomUUID();
    this.title = title;
    this.content = content;
    this.rating = rating;

    if (title == null || content == null || !ratingInRange()) {
      throw DomainError.INVALID_REVIEW;
    }
  }

  private boolean ratingInRange() {
    return rating != null && (10 >= rating) && (rating > 0);
  }
}
