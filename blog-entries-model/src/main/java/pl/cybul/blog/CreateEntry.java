package pl.cybul.blog;

import lombok.EqualsAndHashCode;
import lombok.Value;
import pl.cybul.blog.cqrs.Command;

@EqualsAndHashCode(callSuper = true)
@Value
public class CreateEntry extends Command {

  private String title;
  private String content;
}
