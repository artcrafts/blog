package pl.cybul.blog;

import lombok.EqualsAndHashCode;
import lombok.Value;

import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Value
public class DeleteReview extends BlogCommand {

  private UUID entryId;
  private UUID reviewId;
}
