package pl.cybul.blog;

import pl.cybul.blog.cqrs.Command;

import java.util.UUID;

abstract class BlogCommand extends Command {

  abstract UUID getEntryId();
}
