package pl.cybul.blog;

import lombok.EqualsAndHashCode;
import lombok.Value;

import java.util.UUID;

@EqualsAndHashCode(callSuper = false)
@Value
public final class DomainError extends RuntimeException {

  public static DomainError INVALID_REVIEW = new DomainError(BlogErrors.INVALID_REVIEW);
  public static DomainError ENTRY_NOT_FOUND = new DomainError(BlogErrors.ENTRY_NOT_FOUND);
  public static DomainError REVIEW_ADD_ON_ARCHIVED_ENTRY =
      new DomainError(BlogErrors.REVIEW_ADD_ON_ARCHIVED_ENTRY);
  public static DomainError REVIEW_DELETE_ON_ARCHIVED_ENTRY =
      new DomainError(BlogErrors.REVIEW_DELETE_ON_ARCHIVED_ENTRY);
  public static DomainError ALREADY_ARCHIVED = new DomainError(BlogErrors.ALREADY_ARCHIVED);

  private final String code;
  private final String description;

  private DomainError(BlogErrors error) {
    this.code = error.name();
    this.description = error.getValue();
  }

  public DomainError(BlogErrors error, UUID aggregateId) {
    this.code = error.name();
    this.description = String.format(error.getValue(), aggregateId.toString());
  }
}
