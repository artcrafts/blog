package pl.cybul.blog;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;
import pl.cybul.blog.cqrs.Event;

import java.time.LocalDateTime;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@ToString
@Value
public class BlogEntryReviewDeleted extends Event {

  private final UUID entryId;
  private final UUID reviewId;
  private final LocalDateTime deletionDate;

  @Override
  public UUID getId() {
    return entryId;
  }
}
