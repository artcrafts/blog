package pl.cybul.blog;

import lombok.RequiredArgsConstructor;
import pl.cybul.blog.cqrs.CommandHandler;

@RequiredArgsConstructor
public class PostReviewCommandHandler implements CommandHandler<PostReview> {

  private final BlogRepository blogRepository;
  private final BlogEvents blogEvents;
  private final ObjectFactory objectFactory;

  @Override
  public void handle(PostReview command) {
    BlogEntry entry = blogRepository.getById(command.getEntryId());
    if (entry.notExists()) {
      throw DomainError.ENTRY_NOT_FOUND;
    }
    BlogEntryReview review = objectFactory.create(command);
    entry.addReview(review);

    blogRepository.persist(entry);
    blogEvents.publish(objectFactory.create(entry.getEntryId(), review));
  }
}
