package pl.cybul.blog;

import java.time.LocalDateTime;
import java.util.UUID;

class ObjectFactory {

  BlogEntryCreated create(BlogEntry entry) {
    return new BlogEntryCreated(
        entry.getEntryId(), entry.getTitle(), entry.getContent(), LocalDateTime.now());
  }

  BlogEntryReviewed create(UUID entryId, BlogEntryReview review) {
    return new BlogEntryReviewed(
        entryId,
        review.getReviewId(),
        review.getTitle(),
        review.getContent(),
        review.getRating(),
        LocalDateTime.now());
  }

  BlogEntryReview create(PostReview command) {
    return new BlogEntryReview(command.getTitle(), command.getContent(), command.getRating());
  }

  BlogEntryReviewDeleted create(DeleteReview command) {
    return new BlogEntryReviewDeleted(
        command.getEntryId(), command.getReviewId(), LocalDateTime.now());
  }
}
