package pl.cybul.blog;

public enum EntryState {
  ACTIVE,
  ARCHIVED,
  NOT_EXISTS;

  boolean isArchived() {
    return ARCHIVED.equals(this);
  }
}
