package pl.cybul.blog;

enum BlogErrors {
  ENTRY_NOT_FOUND("Blog entry for given id not exists"),
  REVIEW_ADD_ON_ARCHIVED_ENTRY("Cannot add review for archived entry"),
  REVIEW_DELETE_ON_ARCHIVED_ENTRY("Cannot delete review for archived entry"),
  REVIEW_NOT_FOUND("Cannot find given entryId %s"),
  INVALID_REVIEW("Review object violates constraints (null values or rating range !(10>=x>0)"),
  ALREADY_ARCHIVED("Entry already archived");

  private final String value;

  BlogErrors(String value) {
    this.value = value;
  }

  public String getValue() {
    return this.value;
  }
}
