package pl.cybul.blog;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
public class BlogEntry {

  public static final BlogEntry EMPTY = new BlogEntry();

  private UUID entryId;
  private String title;
  private String content;
  private EntryState state;
  private List<BlogEntryReview> reviews;
  private Double averageRating;

  private BlogEntry() {
    this.entryId = null;
    this.state = EntryState.NOT_EXISTS;
  }

  public BlogEntry(String title, String content) {
    this.entryId = UUID.randomUUID();
    this.title = title;
    this.content = content;
    this.state = EntryState.ACTIVE;
    this.averageRating = 0d;
    this.reviews = new ArrayList<>();
  }

  public boolean notExists() {
    return entryId == null && EntryState.NOT_EXISTS.equals(state);
  }

  public boolean equals(BlogEntry obj) {
    return this.entryId == obj.getEntryId();
  }

  public void archive() {
    if (!ArchivePolicy.archivePossible(this)) {
      throw DomainError.ALREADY_ARCHIVED;
    }
    this.state = EntryState.ARCHIVED;
  }

  public void addReview(BlogEntryReview review) {
    if (!ReviewPolicy.reviewAddPossible(this)) {
      throw DomainError.REVIEW_ADD_ON_ARCHIVED_ENTRY;
    }
    this.reviews.add(review);
    recalculateAverageRating();
  }

  public void deleteReview(UUID reviewId) {
    if (!ReviewPolicy.reviewDeletionPossible(this)) {
      throw DomainError.REVIEW_DELETE_ON_ARCHIVED_ENTRY;
    }
    if (!this.reviews.removeIf(r -> r.getReviewId().equals(reviewId))) {
      throw new DomainError(BlogErrors.REVIEW_NOT_FOUND, reviewId);
    }
    recalculateAverageRating();
  }

  private void recalculateAverageRating() {
    this.averageRating = RatingCalculationRules.recalculateRating(this.reviews);
  }

  public boolean isArchived() {
    return state.isArchived();
  }

  private static class RatingCalculationRules {
    static Double recalculateRating(List<BlogEntryReview> reviews) {
      return reviews.stream().mapToDouble(BlogEntryReview::getRating).average().orElse(0d);
    }
  }
}
