package pl.cybul.blog;

import lombok.RequiredArgsConstructor;
import pl.cybul.blog.cqrs.CommandHandler;

import java.time.LocalDateTime;

@RequiredArgsConstructor
public class ArchiveEntryCommandHandler implements CommandHandler<ArchiveEntry> {

  private final BlogRepository blogRepository;
  private final BlogEvents blogEvents;

  @Override
  public void handle(ArchiveEntry command) {
    BlogEntry entry = blogRepository.getById(command.getEntryId());
    if (entry.notExists()) {
      throw DomainError.ENTRY_NOT_FOUND;
    }
    entry.archive();
    blogRepository.persist(entry);
    blogEvents.publish(new BlogEntryArchived(entry.getEntryId(), LocalDateTime.now()));
  }
}
