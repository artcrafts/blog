package pl.cybul.blog;

import lombok.RequiredArgsConstructor;
import pl.cybul.blog.cqrs.CommandHandler;

@RequiredArgsConstructor
public class DeleteReviewCommandHandler implements CommandHandler<DeleteReview> {

  private final BlogRepository blogRepository;
  private final BlogEvents blogEvents;
  private final ObjectFactory objectFactory;

  @Override
  public void handle(DeleteReview command) {
    BlogEntry entry = blogRepository.getById(command.getEntryId());
    if (entry.notExists()) {
      throw DomainError.ENTRY_NOT_FOUND;
    }
    entry.deleteReview(command.getReviewId());

    blogRepository.persist(entry);
    blogEvents.publish(objectFactory.create(command));
  }
}
