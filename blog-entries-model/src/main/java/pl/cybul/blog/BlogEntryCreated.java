package pl.cybul.blog;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;
import pl.cybul.blog.cqrs.Event;

import java.time.LocalDateTime;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@ToString
@Value
public class BlogEntryCreated extends Event {

  private final UUID entryId;
  private final String title;
  private final String content;
  private final LocalDateTime creationDate;

  @Override
  public UUID getId() {
    return entryId;
  }
}
