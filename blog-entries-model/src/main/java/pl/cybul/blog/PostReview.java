package pl.cybul.blog;

import lombok.EqualsAndHashCode;
import lombok.Value;

import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Value
public class PostReview extends BlogCommand {

  private UUID entryId;
  private String title;
  private String content;
  private Integer rating;
}
