package pl.cybul.blog.entrypoint;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
class PostReviewDto {

  private String title;
  private String content;
  private Integer rating;
}
