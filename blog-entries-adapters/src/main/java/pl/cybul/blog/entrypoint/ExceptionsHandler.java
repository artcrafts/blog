package pl.cybul.blog.entrypoint;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import pl.cybul.blog.DomainError;

@ControllerAdvice
class ExceptionsHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(value = {DomainError.class})
  public ResponseEntity handleDomainError(DomainError exception) {
    return ResponseEntity.badRequest()
        .body(new Result(exception.getCode(), exception.getDescription()));
  }

  @ExceptionHandler(value = {MethodArgumentTypeMismatchException.class})
  public ResponseEntity handleMethodArgumentTypeMismatchException(
      MethodArgumentTypeMismatchException exception) {
    return ResponseEntity.badRequest().body(new Result("INVALID_UUID", exception.getMessage()));
  }
}
