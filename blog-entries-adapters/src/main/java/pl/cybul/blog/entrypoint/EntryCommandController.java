package pl.cybul.blog.entrypoint;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.cybul.blog.ArchiveEntry;
import pl.cybul.blog.CreateEntry;
import pl.cybul.blog.DeleteReview;
import pl.cybul.blog.PostReview;
import pl.cybul.blog.cqrs.CommandBus;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
class EntryCommandController {

  private final CommandBus commandBus;

  @ApiOperation(value = "Creates a blog entry")
  @PostMapping(
      path = "/api/blog-entries",
      produces = {"application/json"})
  public ResponseEntity createEntry(@RequestBody CreateEntry createEntryCommand) {
    commandBus.handle(createEntryCommand);
    return ResponseEntity.ok().build();
  }

  @ApiOperation(value = "Archives a blog entry")
  @PutMapping(
      path = "/api/blog-entries/{entryId}/archives",
      produces = {"application/json"})
  public ResponseEntity archiveEntry(@PathVariable("entryId") UUID entryId) {
    commandBus.handle(new ArchiveEntry(entryId));
    return ResponseEntity.ok().build();
  }

  @ApiOperation(value = "Creates a blog entry review")
  @PostMapping(
      path = "/api/blog-entries/{entryId}/reviews",
      produces = {"application/json"})
  public ResponseEntity postEntryReview(@RequestBody PostReviewDto dto, @PathVariable("entryId") UUID entryId) {
    commandBus.handle(new PostReview(entryId, dto.getTitle(), dto.getContent(), dto.getRating()));
    return ResponseEntity.ok().build();
  }

  @ApiOperation(value = "Deletes a blog entry review")
  @DeleteMapping(
      path = "/api/blog-entries/{entryId}/reviews/{reviewId}",
      produces = {"application/json"})
  public ResponseEntity postEntryReview(
      @PathVariable("entryId") UUID entryId, @PathVariable("reviewId") UUID reviewId) {
    commandBus.handle(new DeleteReview(entryId, reviewId));
    return ResponseEntity.ok().build();
  }
}
