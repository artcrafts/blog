package pl.cybul.blog.entrypoint;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
class Result {
  private String errorCode;
  private String errorDesc;
}
