package pl.cybul.blog.entrypoint;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.cybul.blog.BlogEntry;
import pl.cybul.blog.DomainError;
import pl.cybul.blog.projection.BlogEntryViewService;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
class EntryQueryController {

  private final BlogEntryViewService blogEntryViewService;

  @ApiOperation(value = "Returns all blog entries")
  @GetMapping("/api/blog-entries")
  public ResponseEntity<List<BlogEntry>> getBlogEntries() {
    return ResponseEntity.ok(blogEntryViewService.getAll());
  }

  @ApiOperation(value = "Returns blog entry representation for given id")
  @GetMapping("/api/blog-entries/{entryId}")
  public ResponseEntity getBlogEntryById(@PathVariable("entryId") UUID entryId) {
    BlogEntry entry = blogEntryViewService.getById(entryId);
    if (entry.notExists()) {
      return handleNonExistingEntry();
    }
    return ResponseEntity.ok(entry);
  }

  private ResponseEntity handleNonExistingEntry() {
    DomainError entryNotFound = DomainError.ENTRY_NOT_FOUND;
    return ResponseEntity.badRequest()
        .body(new Result(entryNotFound.getCode(), entryNotFound.getDescription()));
  }
}
