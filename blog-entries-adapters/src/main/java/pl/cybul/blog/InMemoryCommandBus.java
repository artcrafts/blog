package pl.cybul.blog;

import org.springframework.stereotype.Component;
import pl.cybul.blog.cqrs.Command;
import pl.cybul.blog.cqrs.CommandBus;
import pl.cybul.blog.cqrs.CommandHandler;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
class InMemoryCommandBus implements CommandBus {

  private final Map<String, CommandHandler> commandHandlers;

  public InMemoryCommandBus(List<CommandHandler> commandHandlers) {
    this.commandHandlers = new HashMap<>();
    commandHandlers.forEach(
        commandHandler -> {
          ParameterizedType genericClass = resolveParametrizedType(commandHandler);
          String typeName = resolveTypeName(genericClass.getActualTypeArguments()[0]);
          if (this.commandHandlers.containsKey(typeName)) {
            throw new IllegalStateException(
                "Attempt of registering another CommandHandler for Command of: " + typeName);
          }
          this.commandHandlers.put(typeName, commandHandler);
        });
  }

  @Override
  @SuppressWarnings("unchecked")
  public void handle(Command command) {
    commandHandlers.get(resolveTypeName(command.getClass())).handle(command);
  }

  private String resolveTypeName(Type actualTypeArgument) {
    return actualTypeArgument.getTypeName();
  }

  private ParameterizedType resolveParametrizedType(CommandHandler commandHandler) {
    return (ParameterizedType) commandHandler.getClass().getGenericInterfaces()[0];
  }
}
