package pl.cybul.blog;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.cybul.blog.cqrs.CommandHandler;

import java.util.ArrayList;
import java.util.List;

@Configuration
class CommandConfiguration {

  private final BlogRepository blogRepository;
  private final BlogEvents blogEvents;
  private final ObjectFactory objectFactory;

  public CommandConfiguration(BlogRepository blogRepository, BlogEvents blogEvents) {
    this.blogRepository = blogRepository;
    this.blogEvents = blogEvents;
    this.objectFactory = new ObjectFactory();
  }

  /*
      Handlers are registered explicit intentionally - to show that we do not need Spring to define
      Command Handlers containing domain's model behaviour.
      Of course, they could be exposed as beans itself in an adapter module or some additional layer may be used.
  */
  @Bean
  public List<CommandHandler> commandHandlers() {
    List<CommandHandler> handlers = new ArrayList<>();
    handlers.add(new CreateEntryCommandHandler(blogRepository, blogEvents, objectFactory));
    handlers.add(new ArchiveEntryCommandHandler(blogRepository, blogEvents));
    handlers.add(new PostReviewCommandHandler(blogRepository, blogEvents, objectFactory));
    handlers.add(new DeleteReviewCommandHandler(blogRepository, blogEvents, objectFactory));
    return handlers;
  }
}
