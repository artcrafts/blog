package pl.cybul.blog;

import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.stereotype.Component;
import pl.cybul.blog.cqrs.Event;
import pl.cybul.blog.cqrs.EventStore;

/*
 In this POC, the goal of events propagation is to save them to the Event Store (simplified implementation - just Event Log actually)
 In a real world, they may be triggers to do some magic in other parts of the system.
*/
@Log
@RequiredArgsConstructor
@Component
class BlogEventsPropagation implements BlogEvents {

  private final EventStore eventStore;

  @Override
  public void publish(Event event) {
    log.info(event.toString());
    eventStore.storeEvent(event.getId(), event);

    // do some other awesome things, eg. connect Event Handlers
  }
}
