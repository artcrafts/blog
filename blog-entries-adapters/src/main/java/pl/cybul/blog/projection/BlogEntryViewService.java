package pl.cybul.blog.projection;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.cybul.blog.BlogEntry;
import pl.cybul.blog.BlogRepository;

import java.util.List;
import java.util.UUID;

/*
  In a full implementation:
   - there should be some ORM repository
   - service could return different view model (model segregation principle)
  Here, Domain Model = View Model => simplified solution
*/
@Component
@RequiredArgsConstructor
public class BlogEntryViewService {

  private final BlogRepository blogRepository;

  public List<BlogEntry> getAll() {
    return blogRepository.getAll();
  }

  public BlogEntry getById(UUID entryId) {
    return blogRepository.getById(entryId);
  }
}
