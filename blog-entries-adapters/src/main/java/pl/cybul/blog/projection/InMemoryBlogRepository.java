package pl.cybul.blog.projection;

import org.springframework.stereotype.Component;
import pl.cybul.blog.BlogEntry;
import pl.cybul.blog.BlogRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
class InMemoryBlogRepository implements BlogRepository {

  private List<BlogEntry> inMemoryEntries = new ArrayList<>();

  @Override
  public void persist(BlogEntry blogEntry) {
    inMemoryEntries.stream()
        .filter(be -> be.equals(blogEntry))
        .findAny()
        .ifPresentOrElse(
            a ->
                inMemoryEntries =
                    inMemoryEntries.stream()
                        .map(be -> be.equals(blogEntry) ? blogEntry : be)
                        .collect(Collectors.toList()),
            () -> inMemoryEntries.add(blogEntry));
  }

  @Override
  public BlogEntry getById(UUID entryId) {
    return inMemoryEntries.stream()
        .filter(be -> be.getEntryId().equals(entryId))
        .findAny()
        .orElse(BlogEntry.EMPTY);
  }

  @Override
  public List<BlogEntry> getAll() {
    return inMemoryEntries;
  }
}
