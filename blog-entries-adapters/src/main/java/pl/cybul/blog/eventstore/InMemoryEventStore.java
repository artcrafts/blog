package pl.cybul.blog.eventstore;

import lombok.extern.java.Log;
import org.springframework.stereotype.Component;
import pl.cybul.blog.cqrs.Event;
import pl.cybul.blog.cqrs.EventStore;

import java.util.*;

@Log
@Component
class InMemoryEventStore implements EventStore {

  private Map<UUID, List<Event>> inMemoryAggregates = new HashMap<>();

  @Override
  public void storeEvent(UUID aggregateId, Event event) {
    log.info(aggregateId.toString() + " / " + event.getName() + " / " + event.toString());
    if (inMemoryAggregates.containsKey(aggregateId)) {
      inMemoryAggregates.get(aggregateId).add(event);
    } else {
      List<Event> events = new ArrayList<>();
      events.add(event);
      inMemoryAggregates.put(aggregateId, events);
    }
  }

  @Override
  public List<Event> getAggregateEvents(UUID aggregateId) {
    return inMemoryAggregates.get(aggregateId);
  }
}
