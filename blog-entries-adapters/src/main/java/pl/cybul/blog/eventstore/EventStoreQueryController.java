package pl.cybul.blog.eventstore;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.cybul.blog.cqrs.Event;
import pl.cybul.blog.cqrs.EventStore;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
class EventStoreQueryController {

  private final EventStore eventStore;

  @ApiOperation(value = "Returns all blog events")
  @GetMapping("/api/events/{aggregateId}")
  public ResponseEntity<List<Event>> getBlogEntries(@PathVariable("aggregateId") UUID aggregateId) {
    return ResponseEntity.ok(eventStore.getAggregateEvents(aggregateId));
  }
}
