package pl.cybul.blog.cqrs;

public interface CommandBus {

  void handle(Command command);
}
