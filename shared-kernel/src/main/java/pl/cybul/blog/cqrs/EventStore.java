package pl.cybul.blog.cqrs;

import java.util.List;
import java.util.UUID;

public interface EventStore {

  void storeEvent(UUID aggregateId, Event event);

  List<Event> getAggregateEvents(UUID aggregateId);
}
