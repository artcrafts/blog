package pl.cybul.blog.cqrs;

import java.io.Serializable;
import java.util.UUID;

public abstract class Event implements Serializable {

  private String name;

  protected Event() {
    this.name = getClass().getSimpleName();
  }

  public abstract UUID getId();

  public String getName() {
    return name;
  }
}
