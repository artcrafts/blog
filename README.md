The goal of the task is to write a simple web application that is a blogging engine.
The requirements for the system are the following:

* Create blog post (title, content)
* View blog post (title, content, average rating, list of reviews)
* Archive blog post
* Add blog post review (title, content, rating)
* Delete blog post review

The functionalities should be excposed as an API endpoints.
Treat the project as a PoC so e.g. persistent storage is optional.


============================================================

### Prerequisites: 
Java 11+ installed and set as active within OS.
To check current Java version type below in a cli:
```
java -version
```
Expected sample output:
```
openjdk version "11.0.2" 2019-01-15
OpenJDK Runtime Environment 18.9 (build 11.0.2+9)
OpenJDK 64-Bit Server VM 18.9 (build 11.0.2+9, mixed mode)
```

### Run & usage

#### From  cli:
```
./gradlew bootRun -Dspring.profiles.active=local
or
gradlew bootRun -Dspring.profiles.active=local
```
With profile "local" app will start on 8099 port.

Swagger UI is available on path:
```
http://localhost:8099/swagger-ui.html
```

#### From Intellij Idea
Import sources as Gradle project and let Idea to do the job for you :)
